angular.module('grass.input.mask', [
	'grass.utils.input',
	'grass.utils.masker'
])

.directive('gMask', ['gMasker', 'gInputUtils', function(gMasker, gInputUtils) {
	return {
		restrict: "A",
		require: "ngModel",
		replace: false,
		link: function(scope, elem, attrs, ctrl) {
			var _masker = null;
			var _eventsBound = false;
			var _grammar = gInputUtils.grammar;
			var _parse = false;
			var _format = false;

			var _opts = {};

			function init(expression) {
				_masker = new gMasker(expression, _grammar);
				bindEventListeners();
			}

			function initSkipSymbols(skipSymbols) {
				skipSymbols = scope.$eval(skipSymbols);
				_masker.setSkipSymbols(skipSymbols);
			}

			function initParse(parse) {
				_parse = angular.isDefined(parse) ? parse : true;
			}

			function initFormat(format) {
				_format = angular.isDefined(format) ? format : true;
			}

			function bindEventListeners() {
				if(_eventsBound) {return;}

				elem.bind("keyup", eventHandler);
				_eventsBound = true;
			}

			function unbindEventListeners() {
				if(!_eventsBound) {return;}

				elem.unbind("keyup", eventHandler);
				_eventsBound = false;
			}

			function eventHandler(evt) {
				// Prevent the default and do everything manually.
				evt.preventDefault();

				if(gInputUtils.ignoreKeys.indexOf(evt.which) > -1) {return;}

				var val = elem.val();
				var masked = _masker.mask(val);
				var unmasked = _masker.unmask(masked);

				////console.log("############################");
				////console.log("# Key Up");
				////console.log("############################");
				////console.log("|    val = " + val);
				////console.log("|    masked = " + masked);
				////console.log("|    unmasked = " + unmasked);
				////console.log("|    matched = " + _matcher.match(val));
				////console.log("----------------------------");

				elem.val(masked);

				scope.$apply(function (){
					ctrl.$setViewValue(masked);
				});
			}

			// This function format a model value to be presented in the input field.
			function formatter(modelValue) {
				//console.log("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
				//console.log("MASK FORMATTER");
				//console.log("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
				//console.log(modelValue);
				if(!_parse || !_masker) {return modelValue;}

				var masked = _masker.mask(modelValue);

				// Update the unmasked value.
				return masked;
			}

			// This function parse the value presented in the input field to populate the model.
			function parser(viewValue) {
				//console.log("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
				//console.log("MASK PARSER");
				//console.log("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
				//console.log(viewValue);

				if(!_parse || !_masker) {return viewValue;}

				var unmasked = _masker.unmask(viewValue);

				ctrl.$viewValue = unmasked.length ? _masker.mask(unmasked) : '';

				return unmasked;
			}

			// Push formatters and parsers
			ctrl.$formatters.push(formatter);
			ctrl.$parsers.push(parser);

			// Watch for changes
			attrs.$observe("gMask", init);
			attrs.$observe("gMaskSkip", initSkipSymbols);
			attrs.$observe("gMaskParse", initParse);
			attrs.$observe("gMaskFormat", initFormat);

			// Initialize
			init(attrs.gMask);
			initSkipSymbols(attrs.gMaskSkip);
			initParse(attrs.gMaskParse);
			initFormat(attrs.gMaskFormat);
		}
	};
}]);