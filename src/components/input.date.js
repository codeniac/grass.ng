angular.module('grass.input.date', [
	'grass.utils.input'
])

.directive('gDate', ['$locale', 'gInputUtils', function($locale, gInputUtils) {
	return {
		restrict: "A",
		require: "ngModel",
		replace: false,
		link: function(scope, elem, attrs, ctrl) {
			var DEFAULT_FORMATS = {
				view: moment.ISO_8601,
				model: moment.ISO_8601
			};

			var _formats = null;

			function init(formats) {
				formats = (formats ? scope.$eval(formats) : {});
				_formats = angular.extend(DEFAULT_FORMATS, formats);
			}

			function isValid(dateStr) {
				return (dateStr && dateStr.length ? moment(dateStr, _formats.view, true).isValid() : true);
			}

			function modelDate(dateStr) {
				return (dateStr && dateStr.length ? moment(dateStr, _formats.model, true) : false);
			}

			function viewDate(dateStr) {
				return (dateStr && dateStr.length ? moment(dateStr, _formats.view, true) : false);
			}

			// This function format a model value to be presented in the input field.
			function formatter(modelValue) {
				//console.log("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
				//console.log("DATE FORMATTER");
				//console.log("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
				//console.log(modelValue);

				if(!_formats || !modelValue) {return modelValue;}

				var date = modelDate(modelValue);
				var isValid = (date ? date.isValid() : false);
				ctrl.$setValidity('date', isValid);

				//console.log(isValid);

				return (isValid ? date.format(_formats.view) : undefined);
			}

			// This function parse the value presented in the input field to populate the model.
			function parser(viewValue) {
				//console.log("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
				//console.log("DATE PARSER");
				//console.log("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
				//console.log(viewValue);

				if(!_formats || !viewValue) {return viewValue;}

				var date = viewDate(viewValue);
				var isValid = (date ? date.isValid() : false);
				ctrl.$setValidity('date', isValid);

				//console.log(isValid);

				return (isValid ? date.format(_formats.model) : undefined);
			}

			// Push formatters and parsers
			ctrl.$formatters.push(formatter);
			ctrl.$parsers.push(parser);

			// Watch changes
			attrs.$observe("gDate", init);

			// Initialize
			init(attrs.gDate);
		}
	};
}]);