angular.module('grass.notifier', [])

.service('gNotifier', ['$rootScope', function($rootScope) {
	return {
		post: function(type, message) {
			console.log('posting notification!');

			this.type = type;
			this.message = message;

			$rootScope.$broadcast('g:notifier:post');
		},

		clear: function() {
			$rootScope.$broadcast('g:notifier:clear');	
		}
	};
}])

.directive('gNotificationCenter', ['$timeout', 'gNotifier', function($timeout, gNotifier) {
	return {
		replace: true,
		restrict: 'EA',
		template: 
			'<div class="notification-center">' +
				'<div class="box shadow notification" ng-class="notification.type" ng-repeat="notification in notifications">' +
					'<div class="box-header" ng-if="notification.title">' +
						'<div class="notification-title">{{notification.title}}</div>' +
					'</div>' +
					'<div class="box-body">' +
						'{{notification.message}}' +
					'</div>' +
				'</div>' +
			'</div>',

		link: function(scope, elem, attrs, ctrl) {
			scope.notifications = [];

			var Notification = function(type, message) {
				this.id = scope.notifications.length;
				this.type = 'notification-' + type;
				this.message = message;
				this.alreadyPushed = false;

				this.post = function() {
					if(this.alreadyPushed){return;}
					scope.notifications.unshift(this);
					this.alreadyPushed = true;
				};

				this.setTimeout = function() {
					var self = this;
					this.timeout = $timeout(function() {
						self.remove();
					}, 3000);
				};

				this.remove = function() {
					var idx = scope.notifications.indexOf(this);
					if(idx > -1) {scope.notifications.splice(idx, 1);}
				};
			};

			scope.$on('g:notifier:post', function() {
				console.log('notification posted');
				console.log('type = ' + gNotifier.type);
				console.log('message = ' + gNotifier.message);

				var notification = new Notification(gNotifier.type, gNotifier.message);
				notification.post();
				notification.setTimeout();
			});
		}
	};
}]);