angular.module('grass.input.match', [
	'grass.utils.matcher',
	'grass.utils.input'
])

.directive('gMatch', ['gMatcher', 'gInputUtils', function(gMatcher, gInputUtils) {
	return {
		restrict: "A",
		require: "ngModel",
		replace: false,
		link: function(scope, elem, attrs, ctrl) {
			var _matcher = null;
			var _grammar = gInputUtils.grammar;
			var _eventsBound = false;
			var _oldValue = null;

			function init(expression) {
				_matcher = new gMatcher(expression, _grammar);
				bindEventListeners();
			}

			function bindEventListeners() {
				if(_eventsBound) {return;}

				elem.bind("keyup", eventHandler);
				_eventsBound = true;
			}

			function unbindEventListeners() {
				if(!_eventsBound) {return;}

				elem.unbind("keyup", eventHandler);
				_eventsBound = false;
			}

			function eventHandler(evt) {
				// Prevent the default and do everything manually.
				evt.preventDefault();

				if(gInputUtils.ignoreKeys.indexOf(evt.which) > -1) {return;}

				var val = elem.val();
				var matched = _matcher.match(val);

				console.log("############################");
				console.log("# Key Up");
				console.log("############################");
				console.log("|    val = " + val);
				console.log("|    matched = " + matched);
				console.log("----------------------------");

				if(!matched) { elem.val(_oldValue); }

				scope.$apply(function (){
					ctrl.$setViewValue(masked);
				});
			}

			// This function format a model value to be presented in the input field.
			function formatter(modelValue) {
				console.log("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
				console.log("MATCH FORMATTER");
				console.log("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
				console.log(modelValue);

				_oldValue = modelValue;

				return modelValue;
			}

			// Angular binds.
			ctrl.$formatters.push(formatter);

			// Watch for changes
			attrs.$observe("gMatch", init);

			// Initialize
			init(attrs.gMatch);
		}
	};
}]);