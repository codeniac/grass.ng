angular.module('grass.input.number', [])

.directive('gNumber', ['$locale', 'gInputUtils', function($locale, gInputUtils) {
	return {
		restrict: "A",
		require: "ngModel",
		replace: false,
		link: function(scope, elem, attrs, ctrl) {
			var DEFAULT_OPTS = {
				numDecimals: 2,
				prefix: "",
				allowNegative: true,
				allowPositive: false,
				decimalSep: $locale.NUMBER_FORMATS.DECIMAL_SEP,
				groupSep: $locale.NUMBER_FORMATS.GROUP_SEP,
				groupSize: 3,
				sufix: ""
			};

			// Default options.
			var _opts = {};
			var _oldValue = null;
			var _eventsBound = false;

			function init(opts) {
				initOpts(opts);
				bindEventListeners();
			}

			function initOpts(opts) {
				opts = scope.$eval(opts) || {};
				_opts = angular.extend(DEFAULT_OPTS, opts);

				//console.log("OPTIONS");
				//console.log(_opts);
			}

			function clear(text) {
				if(!text){return text;}

				//console.log("==== CLEARING TEXT ====");
				//console.log("    text = " + text);

				var groupSepRegex = new RegExp('\\' + _opts.groupSep, "g");
				var decSepRegex = new RegExp('\\' + _opts.decimalSep, "g");

				//console.log(groupSepRegex);
				//console.log(decSepRegex);

				// Remove all group separators.
				text = text.replace(/\D/g, '');

				//console.log("    cleared = " + text);

				return text;
			}

			function match(chr, text) {
				return (chr == '-' && text.length == 1) || chr.match(/\d/g);
			}

			function insertSeparators(text) {
				if(!text || text.length <= _opts.numDecimals) {return text;}
				//console.log("==== INSERTING SEPARATORS ====");

				var i, result, count;

				if(_opts.numDecimals > 0) {
					i = text.length - _opts.numDecimals - 1;
					result = _opts.decimalSep + text.substring(i+1);
				}
				else {
					i = text.length-1;
					result = "";
				}

				count = 0;

				//console.log("    i = " + i + "; result = " + result);

				while (i >= 0) {

					if(count > 0 && count % _opts.groupSize === 0) {result = _opts.groupSep + result;}
					result = text[i] + result;

					//console.log("    i = " + i + "; result = " + result);
					i--;
					count++;
				}

				return result;
			}

			function mask(text) {
				if(!text) {return text;}

				var sign = '';

				if( (_opts.allowNegative && text[0] == '-') || (_opts.allowPositive && text[0] == '+') ) {
					sign = text[0];
					text = text.substring(1);
				}

				return (_opts.prefix + sign + insertSeparators(clear(text)));
			}

			function unmask(text) {
				if(!text) {return text;}

				if(_opts.prefix.length) {text = text.replace(_opts.prefix, '');}

				var groupSepRegex = new RegExp('\\' + _opts.groupSep, "g");
				var decSepRegex = new RegExp('\\' + _opts.decimalSep, "g");

				// Remove all group separators and normalize decimal separator.
				text = text.replace(groupSepRegex, '').replace(decSepRegex, '.');

				return text;
			}

			function bindEventListeners() {
				if(_eventsBound) {return;}

				elem.bind("keyup", eventHandler);
				_eventsBound = true;
			}

			function unbindEventListeners() {
				if(!_eventsBound) {return;}

				elem.unbind("keyup", eventHandler);
				_eventsBound = false;
			}

			function eventHandler(evt) {
				// Prevent the default and do everything manually.
				evt.preventDefault();

				if(gInputUtils.ignoreKeys.indexOf(evt.which) > -1) {return;}

				var val = elem.val();
				var masked = mask(val);
				var unmasked = unmask(masked);

				//console.log("############################");
				//console.log("# Key Up");
				//console.log("############################");
				//console.log("|    val = " + val);
				//console.log("|    masked = " + masked);
				//console.log("|    unmasked = " + unmasked);
				////console.log("|    matched = " + _matcher.match(val));
				//console.log("----------------------------");

				elem.val(masked);

				scope.$apply(function (){
					ctrl.$setViewValue(masked);
				});
			}


			// This function format a model value to be presented in the input field.
			function formatter(modelValue) {
				//console.log("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
				//console.log("NUMBER FORMATTER");
				//console.log("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
				//console.log(modelValue);

				if(!_eventsBound) {return modelValue;}

				// Update the unmasked value.
				return mask(modelValue);
			}

			// This function parse the value presented in the input field to populate the model.
			function parser(viewValue) {
				//console.log("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
				//console.log("NUMBER PARSER");
				//console.log("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
				//console.log(viewValue);

				if(!_eventsBound) {return viewValue;}

				return unmask(viewValue);
			}

			// Push formatters and parsers
			ctrl.$formatters.push(formatter);
			ctrl.$parsers.push(parser);

			// Watch changes
			attrs.$observe("gNumber", init);

			// Initialize
			init(attrs.gNumber);
		}
	};
}]);