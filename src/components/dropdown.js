/**
 * Taken from angular ui bootstrap project.
 * angular-ui.github.io/bootstrap/
 */
angular.module('grass.dropdown', [
])

.directive('dropdownToggle', ['$document', '$location', function ($document, $location) {
	var openElement = null,
		closeMenu   = angular.noop;

	return {
		restrict: 'CA',
		link: function(scope, element, attrs) {

			// Close the menu when the location changes or the user clicks on the 
			// parent element of the menu-toggle.
			scope.$watch('$location.path', function() { closeMenu(); });
			element.parent().bind('click', function() { closeMenu(); });

			// Bind the click event on the menu-toggle element (it suposed to be 
			// a button or a tag).
			element.bind('click', function (event) {

				var elementWasOpen = (element === openElement);

				// Prevent both default behavior of click event and the propagation of the click
				// to child elements.
				event.preventDefault();
				event.stopPropagation();

				// If there was a toggle which state is open, then close the menu.
				if (!!openElement) {closeMenu();}

				// Open menu.
				if (!elementWasOpen && !element.hasClass('disabled') && !element.prop('disabled')) {
					element.parent().addClass('open');
					openElement = element;
					closeMenu = function (event) {
						if (event) {
							event.preventDefault();
							event.stopPropagation();
						}
						$document.unbind('click', closeMenu);
						element.parent().removeClass('open');
						closeMenu = angular.noop;
						openElement = null;
					};
					$document.bind('click', closeMenu);
				}
			});
		}
	};
}]);