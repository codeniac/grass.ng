angular.module('grass.utils.graph', [

])

//##############################################################
// Directed Graph
//##############################################################
.factory('gDirectedGraph', function() {

	var gDirectedGraph = function(numVertices) {
		var _adj = [];
		var _numEdges = 0;
		var _numVertices = numVertices || 0;

		//-----------------------------------------------------
		//	Instance Functions
		//-----------------------------------------------------
		this.addEdge = function(vIdx, uIdx) {
			_adj[vIdx].push(uIdx);
			_numEdges++;
		};

		this.numVertices = function() {
			return _numVertices;
		};

		this.adj = function(vIdx) {
			return (vIdx >= 0 && vIdx < _numVertices ? _adj[vIdx] : []);
		};

		this.print = function() {
			console.log("############################");
			console.log("# Printing Graph");
			console.log("############################");
			console.log("|    numVertices = " + _numVertices);
			console.log("|    numEdges = " + _numEdges);

			for(var i = 0; i < _numVertices; i++) {
				console.log("|        v[" + i + "] = " + _adj[i].join(", "));
			}
		};

		//-----------------------------------------------------
		//	Initialization
		//-----------------------------------------------------
		for(var i = 0; i < numVertices; i++) {
			_adj.push([]);
		}
	};

	return gDirectedGraph;
})

.factory('gDirectedDFS', function() {
	var gDirectedDFS = function(graph, sources) {
		console.log("############################");
		console.log("# Direct DFS");
		console.log("############################");

		var _graph = graph;
		var _sources = (sources instanceof Array ? sources : [sources]);
		var _marked = [];

		console.log("|    sources = " + _sources.join(", "));
		console.log("");
		
		//-----------------------------------------------------
		//	Private Functions
		//-----------------------------------------------------
		var _dfs = function(vIdx) {
			var adj = _graph.adj(vIdx);

			console.log("|        dfs for " + vIdx);
			console.log("|            Adjs = " + adj.join(", "));

			_marked[vIdx] = true;

			for( var i = 0; i < adj.length; i++ ) {
				if(!_marked[adj[i]]) {_dfs(adj[i]);}
			}
		};

		//-----------------------------------------------------
		//	Instance Functions
		//-----------------------------------------------------
		this.marked = function(vIdx) {
			return (vIdx >= 0 && vIdx < _graph.numVertices() ? _marked[vIdx] : undefined);
		};

		
		//-----------------------------------------------------
		//	Initialization
		//-----------------------------------------------------
		// Initializing _marked array.
		for(var i = 0; i < _graph.numVertices(); i++) {
			_marked.push(false);
		}

		// Search
		for(i = 0; i < _sources.length; i++) {
			console.log("|    src " + i + ":");

			if(!_marked[_sources[i]]) {
				console.log("|        Not Marked!");
				_dfs(_sources[i]);
			}
		}
	};

	return gDirectedDFS;
});