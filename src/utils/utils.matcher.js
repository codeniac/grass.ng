angular.module('grass.utils.matcher', [
	'grass.utils.graph'
])

.factory('gMatcherUtils', function() {
	return {
		TAGS: {
			start:		0,
			end:		1,
			closure:	2,
			optional:	3,
			or:			4,
			pattern:	5,
			symbol:		6
		},

		findGrammarPattern: function(chr, grammar) {
			var pattern = false;

			angular.forEach(grammar.patterns, function(value, key) {
				if(chr == key) {
					pattern = value;
				}
			});

			return pattern;
		},

		tokenize: function(expression, grammar, symbols) {
			console.log("|    Tokenizing:");
			var tokens = [];

			for(var i = 0; i < expression.length; i++) {
				var chr = expression[i];
				var value = null;

				console.log("|        chr = " + chr);

				if(chr == grammar.group.start) {
					console.log("|            Start");
					tokens.push({
						tag: this.TAGS.start
					});
				}
				else if(chr == grammar.group.end) {
					console.log("|            End");
					tokens.push({
						tag: this.TAGS.end
					});
				}
				else if(chr == grammar.closure) {
					console.log("|            Closure");
					tokens.push({
						tag: this.TAGS.closure
					});
				}
				else if(chr == grammar.optional) {
					console.log("|            Optional");
					tokens.push({
						tag: this.TAGS.optional
					});
				}
				else if(chr == grammar.or) {
					console.log("|            Or");
					tokens.push({
						tag: this.TAGS.or
					});
				}
				else if(value = this.findGrammarPattern(chr, grammar)) {
					console.log("|            Pattern");
					tokens.push({
						tag: this.TAGS.pattern,
						value: value
					});
				}
				else {
					console.log("|            Symbol");

					if(symbols) {symbols.push(chr);}
					
					tokens.push({
						tag: this.TAGS.symbol,
						value: chr
					});
				}
			}

			return tokens;
		},

		parse: function(tokens) {
			var graph = new DirectedGraph(tokens.length+1);
			var stack = [];

			for(var i = 0; i < tokens.length; i++) {
				var lp = i;

				if(tokens[i].tag == this.TAGS.start || 
					tokens[i].tag == this.TAGS.or) {
					stack.push(i);
				}
				else if(tokens[i].tag == this.TAGS.end) {
					var or = stack.pop();

					if(tokens[or].tag == this.TAGS.or) {
						lp = stack.pop();
						graph.addEdge(lp, or+1);
						graph.addEdge(or,i);
					}
					else { lp = or; }
				}

				// Lookahead
				if(i < (tokens.length - 1)) {
					if(tokens[i+1].tag == this.TAGS.closure) {
						graph.addEdge(lp, i+1);
						graph.addEdge(i+1, lp);
					}
					else if(tokens[i+1].tag == this.TAGS.optional) {
						graph.addEdge(lp, i+1);
					}
				}

				if( tokens[i].tag == this.TAGS.start || 
					tokens[i].tag == this.TAGS.closure || 
					tokens[i].tag == this.TAGS.optional || 
					tokens[i].tag == this.TAGS.end ) { graph.addEdge(i, i+1); }
			}

			return graph;
		},

		parseReverse: function(tokens) {
			console.log("REVERSE PARSING!");

			var graph = new DirectedGraph(tokens.length+1);
			var stack = [];
			var rev_stack = [];

			for(var i = tokens.length; i > 0; i--) {
				console.log('node = ' + i);

				var rp = i;

				if( tokens[i-1].tag == this.TAGS.end && (i-1) < tokens.length-1 && (tokens[i].tag == this.TAGS.closure || tokens[i].tag == this.TAGS.optional) ) {
					console.log('    end + clousure/optional');

					stack.push(i);
					stack.push(i+1);

					console.log('        stack = ' + stack.join(', '));
				}
				else if( tokens[i-1].tag == this.TAGS.end || tokens[i-1].tag == this.TAGS.or ) {
					console.log('    end/or');

					stack.push(i);

					console.log('        stack = ' + stack.join(', '));
				}
				else if(tokens[i-1].tag == this.TAGS.start) {
					console.log('    start');
					console.log('        stack = ' + stack.join(', '));

					var or = false;
					var op = stack.pop();

					if(tokens[op-1].tag == this.TAGS.or) {
						console.log('            op = or');
						console.log('                edge = (' + op + ', ' + i + ')');

						graph.addEdge(op, i);
						or = op;
						op = stack.pop();

						console.log('        stack = ' + stack.join(', '));
					}
					
					if(tokens[op-1].tag == this.TAGS.closure) {
						console.log('            op = closure');
						console.log('                edge = (' + op + ', ' + i + ')');
						console.log('                edge = (' + i + ', ' + op + ')');

						graph.addEdge(op, i);
						graph.addEdge(i, op);
						op = stack.pop();

						console.log('        stack = ' + stack.join(', '));
					}
					
					if(tokens[op-1].tag == this.TAGS.optional) {
						console.log('            op = optional');
						console.log('                edge = (' + op + ', ' + i + ')');

						graph.addEdge(op, i);
						op = stack.pop();

						console.log('        stack = ' + stack.join(', '));
					}

					if(tokens[op-1].tag == this.TAGS.end) {
						console.log('            op = end');
						if(or) {console.log('                has or! edge = (' + op + ', ' + (or-1) + ')'); graph.addEdge(op, or-1);}
						rp = op;
					}
				}


				if( tokens[i-1].tag == this.TAGS.end || 
					tokens[i-1].tag == this.TAGS.closure || 
					tokens[i-1].tag == this.TAGS.optional || 
					tokens[i-1].tag == this.TAGS.start ) { graph.addEdge(i, i-1); }
			}

			return graph;
		}
	};
})

.factory('gMatcher', ['gMatcherUtils', function(gMatcherUtils) {
	var gMatcher = function(expression, grammar) {
		var _grammar = grammar;
		var _tokens = gMatcherUtils.tokenize(expression, _grammar);
		var _graph = gMatcherUtils.parse(_tokens);						// non deterministic state machine with epsilon transitions only.

		_graph.print();

		//-----------------------------------------------------
		//	Instance Functions
		//-----------------------------------------------------
		// Match a text against this regex.
		this.match = function(text) {
			//console.log("############################");
			//console.log("# Matching");
			//console.log("############################");
			//console.log("|    text = " + text);

			var i, j, v, w;

			// Get states reachable from start through epsilon transitions.
			var stateIdxs = [];
			var dfs = new DirectedDFS(_graph, 0);

			for(v = 0; v < _graph.numVertices(); v++) {
				if(dfs.marked(v)) {stateIdxs.push(v);}
			}

			//console.log("|    Reachable = " + stateIdxs.join(", "));

			// Iterate over the text characters.
			for(i = 0; i < text.length; i++) {
				var matched = [];

				//console.log("|        text[" + i + "] = " + text[i]);

				// Search for matches within current reachable states.
				for(j = 0; j < stateIdxs.length; j++) {
					v = stateIdxs[j];

					//console.log("|            state = " + v);
					if(v < _tokens.length) {
						//console.log("|                < _tokens.length");

						if(_tokens[v].tag == gMatcherUtils.TAGS.pattern && text[i].match(_tokens[v].value)) {
							//console.log("|                pattern matched: " + _tokens[v].value);
							matched.push(v+1);
						}
						else if(_tokens[v].tag == gMatcherUtils.TAGS.symbol && text[i] == _tokens[v].value) {
							//console.log("|                symbol matched: " + _tokens[v].value);
							matched.push(v+1);
						}
					}
				}

				//console.log("|        Matched Array = " + matched.join(", "));

				// Get states reachable from the matched states through epsilon transitions.
				stateIdxs = [];
				dfs = new DirectedDFS(_graph, matched);

				for(v = 0; v < _graph.numVertices(); v++) {
					if(dfs.marked(v)) {stateIdxs.push(v);}
				}

				//console.log("|    Reachable = " + stateIdxs.join(", "));
			}

			// Check if there is a accepted state within final reachable states.
			for(i = 0; i < stateIdxs.length; i++) {
				if(stateIdxs[i] == _tokens.length) {return true;}
			}

			return false;
		};
	};

	return gMatcher;
}]);