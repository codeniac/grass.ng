angular.module('grass.utils.masker', [])

.factory('gMaskerUtils', function() {
	return {
		TAGS: {
			pattern: 'pattern',
			symbol: 'symbol'
		},

		options: {
			ignoreKeys: [16,17,18,37,38,39,40,91]
		},

		findGrammarPattern: function(chr, grammar) {
			for (var key in grammar) {
				if (grammar.hasOwnProperty(key) && chr == key) {
					return grammar[key];
				}
			}

			return false;
		},

		tokenize: function(expression, grammar) {
			var tokens = [];

			for(var i = 0; i < expression.length; i++) {
				var chr = expression[i];
				var value = null;

				if(value = this.findGrammarPattern(chr, grammar)) {
					tokens.push({
						tag: this.TAGS.pattern,
						pattern: value
					});
				}
				else {
					tokens.push({
						tag: this.TAGS.symbol,
						symbol: chr
					});
				}
			}

			return tokens;
		}
	};
})

.factory('gMasker', ['gMaskerUtils', function(gMaskerUtils) {
	var gMasker = function(expression, grammar, skipSymbols) {
		var _grammar = grammar;
		var _skipSymbols = (skipSymbols || []);
		var _tokens = gMaskerUtils.tokenize(expression, grammar);

		console.log(_tokens);

		this.mask = function(text) {
			if(!text) {return "";}

			console.log("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
			console.log("Masking");
			console.log("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");

			var i = 0, j = 0, masked = "";

			while(i < text.length && j < _tokens.length) {
				var chr = text[i];

				if( (_tokens[j].tag == gMaskerUtils.TAGS.pattern && chr.match(_tokens[j].pattern.definition)) ||
					(_tokens[j].tag == gMaskerUtils.TAGS.symbol && chr == _tokens[j].symbol)	) {
					masked += chr;
					i++;
				}
				else if( _tokens[j].tag == gMaskerUtils.TAGS.symbol ) {
					masked += _tokens[j].symbol; // Não move texto. Move pattern.
				}
				else {
					return masked;
				}

				j++;
			}

			return masked;
		};

		this.unmask = function(text) {
			if(!text) {return "";}

			var i = 0, j = 0, unmasked = "";

			console.log("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
			console.log("UnMasking");
			console.log("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");

			// Remove symbols
			while(i < text.length && j < _tokens.length) {
				var chr = text[i];

				console.log("    text[" + i + "] = " + chr);

				if(_tokens[j].tag != gMaskerUtils.TAGS.symbol || _skipSymbols.indexOf(chr) >= 0 ) {
					console.log("        pattern || skiped");
					unmasked += chr;
				}

				console.log("        unmasked = " + unmasked);

				i++; j++;
			}

			return unmasked;
		};

		this.setSkipSymbols = function(skipSymbols) {
			if(typeof skipSymbols == 'string') { skipSymbols = [skipSymbols];}

			_skipSymbols = skipSymbols || [];
		};
	};

	return gMasker;
}]);