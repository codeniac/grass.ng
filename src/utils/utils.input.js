angular.module('grass.utils.input', [
])

.value('gInputUtils', {
	ignoreKeys: [
		16, // Shift
		17,	// Ctrl
		18,	// Alt
		20, // Caps Lock
		37,	// Left Arrow
		38,	// Up Arrow
		39,	// Right Arrow
		40,	// Down Arrow
		91,	// Left Window
		92, // Right Window

	],

	grammar: {
		'1': { definition: /[1-9]/ },
		'9': { definition: /[0-9]/ },
		'a': { definition: /[a-zA-Z]/},
		'#': { definition: /[a-zA-Z0-9]/}
	}
});