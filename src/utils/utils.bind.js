/**
 * Taken from angular ui bootstrap project.
 * angular-ui.github.io/bootstrap/
 */
angular.module('grass.utils.bind', [])

.directive('bindHtmlUnsafe', function () {
	return function (scope, element, attr) {
		element.addClass('ng-binding').data('$binding', attr.bindHtmlUnsafe);
		scope.$watch(attr.bindHtmlUnsafe, function bindHtmlUnsafeWatchAction(value) {
			element.html(value || '');
		});
	};
});