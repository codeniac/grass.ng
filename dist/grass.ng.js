/**
 * grass.ng - v0.0.4
 * http://grass.codeniac.com
 *
 * Created by Bruno Fonseca on 2014-09-02
 * Copyright (c) 2014 Codeniac. All rights reserved.
 */
angular.module('grass', [
	'ngAnimate',
	'grass.utils',
	'grass.tpls',
	'grass.collapse',
	'grass.dropdown',
	'grass.modal',
	'grass.tooltip',
	'grass.input.match',
	'grass.input.mask',
	'grass.input.number',
	'grass.input.date',
	'grass.notifier',
	'grass.progressbar',
	'grass.buttons'
]);

angular.module('grass.utils', [
	'grass.utils.transition',
	'grass.utils.position',
	'grass.utils.bind',
	'grass.utils.graph',
	'grass.utils.input',
	'grass.utils.matcher',
	'grass.utils.masker'
]);

angular.module('grass.tpls', [
	'templates/modal/backdrop.html',
	'templates/modal/window.html',
	'templates/tooltip/tooltip-popup.html',
	'templates/tooltip/tooltip-html-unsafe-popup.html',
	'templates/progressbar/progress.html',
	'templates/progressbar/bar.html',
	'templates/progressbar/progressbar.html'
]);
/**
 * Taken from angular ui bootstrap project.
 * httpangular-ui.github.io/bootstrap/
 */

angular.module('grass.buttons', [])

.constant('buttonConfig', {
	activeClass: 'active',
	toggleEvent: 'click'
})

.controller('ButtonsController', ['buttonConfig', function(buttonConfig) {
	this.activeClass = buttonConfig.activeClass || 'active';
	this.toggleEvent = buttonConfig.toggleEvent || 'click';
}])

.directive('btnRadio', function () {
	return {
		require: ['btnRadio', 'ngModel'],
		controller: 'ButtonsController',
		link: function (scope, element, attrs, ctrls) {
			var buttonsCtrl = ctrls[0], ngModelCtrl = ctrls[1];

			//model -> UI
			ngModelCtrl.$render = function () {
				element.toggleClass(buttonsCtrl.activeClass, angular.equals(ngModelCtrl.$modelValue, scope.$eval(attrs.btnRadio)));
			};

			//ui->model
			element.bind(buttonsCtrl.toggleEvent, function () {
				var isActive = element.hasClass(buttonsCtrl.activeClass);

				if (!isActive || angular.isDefined(attrs.uncheckable)) {
					scope.$apply(function () {
						ngModelCtrl.$setViewValue(isActive ? null : scope.$eval(attrs.btnRadio));
						ngModelCtrl.$render();
					});
				}
			});
		}
	};
})

.directive('btnCheckbox', function () {
	return {
		require: ['btnCheckbox', 'ngModel'],
		controller: 'ButtonsController',
		link: function (scope, element, attrs, ctrls) {
			var buttonsCtrl = ctrls[0], ngModelCtrl = ctrls[1];

			function getTrueValue() {
				return getCheckboxValue(attrs.btnCheckboxTrue, true);
			}

			function getFalseValue() {
				return getCheckboxValue(attrs.btnCheckboxFalse, false);
			}

			function getCheckboxValue(attributeValue, defaultValue) {
				var val = scope.$eval(attributeValue);
				return angular.isDefined(val) ? val : defaultValue;
			}

			//model -> UI
			ngModelCtrl.$render = function () {
				element.toggleClass(buttonsCtrl.activeClass, angular.equals(ngModelCtrl.$modelValue, getTrueValue()));
			};

			//ui->model
			element.bind(buttonsCtrl.toggleEvent, function () {
				scope.$apply(function () {
					ngModelCtrl.$setViewValue(element.hasClass(buttonsCtrl.activeClass) ? getFalseValue() : getTrueValue());
					ngModelCtrl.$render();
				});
			});
		}
	};
});

/**
 * Taken from angular ui bootstrap project.
 * httpangular-ui.github.io/bootstrap/
 */
angular.module('grass.collapse', ['grass.utils.transition'])

.directive('collapse', ['$transition', function ($transition, $timeout) {

	return {
		link: function (scope, element, attrs) {

			var initialAnimSkip = true;
			var currentTransition;

			function doTransition(change) {
				var newTransition = $transition(element, change);
				if (currentTransition) {
					currentTransition.cancel();
				}
				currentTransition = newTransition;
				newTransition.then(newTransitionDone, newTransitionDone);
				return newTransition;

				function newTransitionDone() {
					// Make sure it's this transition, otherwise, leave it alone.
					if (currentTransition === newTransition) {
						currentTransition = undefined;
					}
				}
			}

			function expand() {
				if (initialAnimSkip) {
					initialAnimSkip = false;
					expandDone();
				} else {
					element.removeClass('collapse').addClass('collapsing');
					doTransition({ height: element[0].scrollHeight + 'px' }).then(expandDone);
				}
			}

			function expandDone() {
				element.removeClass('collapsing');
				element.addClass('collapse in');
				element.css({height: 'auto'});
			}

			function collapse() {
				if (initialAnimSkip) {
					initialAnimSkip = false;
					collapseDone();
					element.css({height: 0});
				} else {
					// CSS transitions don't work with height: auto, so we have to manually change the height to a specific value
					element.css({ height: element[0].scrollHeight + 'px' });
					//trigger reflow so a browser realizes that height was updated from auto to a specific value
					var x = element[0].offsetWidth;

					element.removeClass('collapse in').addClass('collapsing');

					doTransition({ height: 0 }).then(collapseDone);
				}
			}

			function collapseDone() {
				element.removeClass('collapsing');
				element.addClass('collapse');
			}

			scope.$watch(attrs.collapse, function (shouldCollapse) {
				if (shouldCollapse) {
					collapse();
				} else {
					expand();
				}
			});
		}
	};
}]);
/**
 * Taken from angular ui bootstrap project.
 * angular-ui.github.io/bootstrap/
 */
angular.module('grass.dropdown', [
])

.directive('dropdownToggle', ['$document', '$location', function ($document, $location) {
	var openElement = null,
		closeMenu   = angular.noop;

	return {
		restrict: 'CA',
		link: function(scope, element, attrs) {

			// Close the menu when the location changes or the user clicks on the 
			// parent element of the menu-toggle.
			scope.$watch('$location.path', function() { closeMenu(); });
			element.parent().bind('click', function() { closeMenu(); });

			// Bind the click event on the menu-toggle element (it suposed to be 
			// a button or a tag).
			element.bind('click', function (event) {

				var elementWasOpen = (element === openElement);

				// Prevent both default behavior of click event and the propagation of the click
				// to child elements.
				event.preventDefault();
				event.stopPropagation();

				// If there was a toggle which state is open, then close the menu.
				if (!!openElement) {closeMenu();}

				// Open menu.
				if (!elementWasOpen && !element.hasClass('disabled') && !element.prop('disabled')) {
					element.parent().addClass('open');
					openElement = element;
					closeMenu = function (event) {
						if (event) {
							event.preventDefault();
							event.stopPropagation();
						}
						$document.unbind('click', closeMenu);
						element.parent().removeClass('open');
						closeMenu = angular.noop;
						openElement = null;
					};
					$document.bind('click', closeMenu);
				}
			});
		}
	};
}]);
angular.module('grass.input.date', [
	'grass.utils.input'
])

.directive('gDate', ['$locale', 'gInputUtils', function($locale, gInputUtils) {
	return {
		restrict: "A",
		require: "ngModel",
		replace: false,
		link: function(scope, elem, attrs, ctrl) {
			var DEFAULT_FORMATS = {
				view: moment.ISO_8601,
				model: moment.ISO_8601
			};

			var _formats = null;

			function init(formats) {
				formats = (formats ? scope.$eval(formats) : {});
				_formats = angular.extend(DEFAULT_FORMATS, formats);
			}

			function isValid(dateStr) {
				return (dateStr && dateStr.length ? moment(dateStr, _formats.view, true).isValid() : true);
			}

			function modelDate(dateStr) {
				return (dateStr && dateStr.length ? moment(dateStr, _formats.model, true) : false);
			}

			function viewDate(dateStr) {
				return (dateStr && dateStr.length ? moment(dateStr, _formats.view, true) : false);
			}

			// This function format a model value to be presented in the input field.
			function formatter(modelValue) {
				//console.log("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
				//console.log("DATE FORMATTER");
				//console.log("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
				//console.log(modelValue);

				if(!_formats || !modelValue) {return modelValue;}

				var date = modelDate(modelValue);
				var isValid = (date ? date.isValid() : false);
				ctrl.$setValidity('date', isValid);

				//console.log(isValid);

				return (isValid ? date.format(_formats.view) : undefined);
			}

			// This function parse the value presented in the input field to populate the model.
			function parser(viewValue) {
				//console.log("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
				//console.log("DATE PARSER");
				//console.log("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
				//console.log(viewValue);

				if(!_formats || !viewValue) {return viewValue;}

				var date = viewDate(viewValue);
				var isValid = (date ? date.isValid() : false);
				ctrl.$setValidity('date', isValid);

				//console.log(isValid);

				return (isValid ? date.format(_formats.model) : undefined);
			}

			// Push formatters and parsers
			ctrl.$formatters.push(formatter);
			ctrl.$parsers.push(parser);

			// Watch changes
			attrs.$observe("gDate", init);

			// Initialize
			init(attrs.gDate);
		}
	};
}]);
angular.module('grass.input.mask', [
	'grass.utils.input',
	'grass.utils.masker'
])

.directive('gMask', ['gMasker', 'gInputUtils', function(gMasker, gInputUtils) {
	return {
		restrict: "A",
		require: "ngModel",
		replace: false,
		link: function(scope, elem, attrs, ctrl) {
			var _masker = null;
			var _eventsBound = false;
			var _grammar = gInputUtils.grammar;
			var _parse = false;
			var _format = false;

			var _opts = {};

			function init(expression) {
				_masker = new gMasker(expression, _grammar);
				bindEventListeners();
			}

			function initSkipSymbols(skipSymbols) {
				skipSymbols = scope.$eval(skipSymbols);
				_masker.setSkipSymbols(skipSymbols);
			}

			function initParse(parse) {
				_parse = angular.isDefined(parse) ? parse : true;
			}

			function initFormat(format) {
				_format = angular.isDefined(format) ? format : true;
			}

			function bindEventListeners() {
				if(_eventsBound) {return;}

				elem.bind("keyup", eventHandler);
				_eventsBound = true;
			}

			function unbindEventListeners() {
				if(!_eventsBound) {return;}

				elem.unbind("keyup", eventHandler);
				_eventsBound = false;
			}

			function eventHandler(evt) {
				// Prevent the default and do everything manually.
				evt.preventDefault();

				if(gInputUtils.ignoreKeys.indexOf(evt.which) > -1) {return;}

				var val = elem.val();
				var masked = _masker.mask(val);
				var unmasked = _masker.unmask(masked);

				////console.log("############################");
				////console.log("# Key Up");
				////console.log("############################");
				////console.log("|    val = " + val);
				////console.log("|    masked = " + masked);
				////console.log("|    unmasked = " + unmasked);
				////console.log("|    matched = " + _matcher.match(val));
				////console.log("----------------------------");

				elem.val(masked);

				scope.$apply(function (){
					ctrl.$setViewValue(masked);
				});
			}

			// This function format a model value to be presented in the input field.
			function formatter(modelValue) {
				//console.log("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
				//console.log("MASK FORMATTER");
				//console.log("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
				//console.log(modelValue);
				if(!_parse || !_masker) {return modelValue;}

				var masked = _masker.mask(modelValue);

				// Update the unmasked value.
				return masked;
			}

			// This function parse the value presented in the input field to populate the model.
			function parser(viewValue) {
				//console.log("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
				//console.log("MASK PARSER");
				//console.log("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
				//console.log(viewValue);

				if(!_parse || !_masker) {return viewValue;}

				var unmasked = _masker.unmask(viewValue);

				ctrl.$viewValue = unmasked.length ? _masker.mask(unmasked) : '';

				return unmasked;
			}

			// Push formatters and parsers
			ctrl.$formatters.push(formatter);
			ctrl.$parsers.push(parser);

			// Watch for changes
			attrs.$observe("gMask", init);
			attrs.$observe("gMaskSkip", initSkipSymbols);
			attrs.$observe("gMaskParse", initParse);
			attrs.$observe("gMaskFormat", initFormat);

			// Initialize
			init(attrs.gMask);
			initSkipSymbols(attrs.gMaskSkip);
			initParse(attrs.gMaskParse);
			initFormat(attrs.gMaskFormat);
		}
	};
}]);
angular.module('grass.input.match', [
	'grass.utils.matcher',
	'grass.utils.input'
])

.directive('gMatch', ['gMatcher', 'gInputUtils', function(gMatcher, gInputUtils) {
	return {
		restrict: "A",
		require: "ngModel",
		replace: false,
		link: function(scope, elem, attrs, ctrl) {
			var _matcher = null;
			var _grammar = gInputUtils.grammar;
			var _eventsBound = false;
			var _oldValue = null;

			function init(expression) {
				_matcher = new gMatcher(expression, _grammar);
				bindEventListeners();
			}

			function bindEventListeners() {
				if(_eventsBound) {return;}

				elem.bind("keyup", eventHandler);
				_eventsBound = true;
			}

			function unbindEventListeners() {
				if(!_eventsBound) {return;}

				elem.unbind("keyup", eventHandler);
				_eventsBound = false;
			}

			function eventHandler(evt) {
				// Prevent the default and do everything manually.
				evt.preventDefault();

				if(gInputUtils.ignoreKeys.indexOf(evt.which) > -1) {return;}

				var val = elem.val();
				var matched = _matcher.match(val);

				console.log("############################");
				console.log("# Key Up");
				console.log("############################");
				console.log("|    val = " + val);
				console.log("|    matched = " + matched);
				console.log("----------------------------");

				if(!matched) { elem.val(_oldValue); }

				scope.$apply(function (){
					ctrl.$setViewValue(masked);
				});
			}

			// This function format a model value to be presented in the input field.
			function formatter(modelValue) {
				console.log("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
				console.log("MATCH FORMATTER");
				console.log("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
				console.log(modelValue);

				_oldValue = modelValue;

				return modelValue;
			}

			// Angular binds.
			ctrl.$formatters.push(formatter);

			// Watch for changes
			attrs.$observe("gMatch", init);

			// Initialize
			init(attrs.gMatch);
		}
	};
}]);
angular.module('grass.input.number', [])

.directive('gNumber', ['$locale', 'gInputUtils', function($locale, gInputUtils) {
	return {
		restrict: "A",
		require: "ngModel",
		replace: false,
		link: function(scope, elem, attrs, ctrl) {
			var DEFAULT_OPTS = {
				numDecimals: 2,
				prefix: "",
				allowNegative: true,
				allowPositive: false,
				decimalSep: $locale.NUMBER_FORMATS.DECIMAL_SEP,
				groupSep: $locale.NUMBER_FORMATS.GROUP_SEP,
				groupSize: 3,
				sufix: ""
			};

			// Default options.
			var _opts = {};
			var _oldValue = null;
			var _eventsBound = false;

			function init(opts) {
				initOpts(opts);
				bindEventListeners();
			}

			function initOpts(opts) {
				opts = scope.$eval(opts) || {};
				_opts = angular.extend(DEFAULT_OPTS, opts);

				//console.log("OPTIONS");
				//console.log(_opts);
			}

			function clear(text) {
				if(!text){return text;}

				//console.log("==== CLEARING TEXT ====");
				//console.log("    text = " + text);

				var groupSepRegex = new RegExp('\\' + _opts.groupSep, "g");
				var decSepRegex = new RegExp('\\' + _opts.decimalSep, "g");

				//console.log(groupSepRegex);
				//console.log(decSepRegex);

				// Remove all group separators.
				text = text.replace(/\D/g, '');

				//console.log("    cleared = " + text);

				return text;
			}

			function match(chr, text) {
				return (chr == '-' && text.length == 1) || chr.match(/\d/g);
			}

			function insertSeparators(text) {
				if(!text || text.length <= _opts.numDecimals) {return text;}
				//console.log("==== INSERTING SEPARATORS ====");

				var i, result, count;

				if(_opts.numDecimals > 0) {
					i = text.length - _opts.numDecimals - 1;
					result = _opts.decimalSep + text.substring(i+1);
				}
				else {
					i = text.length-1;
					result = "";
				}

				count = 0;

				//console.log("    i = " + i + "; result = " + result);

				while (i >= 0) {

					if(count > 0 && count % _opts.groupSize === 0) {result = _opts.groupSep + result;}
					result = text[i] + result;

					//console.log("    i = " + i + "; result = " + result);
					i--;
					count++;
				}

				return result;
			}

			function mask(text) {
				if(!text) {return text;}

				var sign = '';

				if( (_opts.allowNegative && text[0] == '-') || (_opts.allowPositive && text[0] == '+') ) {
					sign = text[0];
					text = text.substring(1);
				}

				return (_opts.prefix + sign + insertSeparators(clear(text)));
			}

			function unmask(text) {
				if(!text) {return text;}

				if(_opts.prefix.length) {text = text.replace(_opts.prefix, '');}

				var groupSepRegex = new RegExp('\\' + _opts.groupSep, "g");
				var decSepRegex = new RegExp('\\' + _opts.decimalSep, "g");

				// Remove all group separators and normalize decimal separator.
				text = text.replace(groupSepRegex, '').replace(decSepRegex, '.');

				return text;
			}

			function bindEventListeners() {
				if(_eventsBound) {return;}

				elem.bind("keyup", eventHandler);
				_eventsBound = true;
			}

			function unbindEventListeners() {
				if(!_eventsBound) {return;}

				elem.unbind("keyup", eventHandler);
				_eventsBound = false;
			}

			function eventHandler(evt) {
				// Prevent the default and do everything manually.
				evt.preventDefault();

				if(gInputUtils.ignoreKeys.indexOf(evt.which) > -1) {return;}

				var val = elem.val();
				var masked = mask(val);
				var unmasked = unmask(masked);

				//console.log("############################");
				//console.log("# Key Up");
				//console.log("############################");
				//console.log("|    val = " + val);
				//console.log("|    masked = " + masked);
				//console.log("|    unmasked = " + unmasked);
				////console.log("|    matched = " + _matcher.match(val));
				//console.log("----------------------------");

				elem.val(masked);

				scope.$apply(function (){
					ctrl.$setViewValue(masked);
				});
			}


			// This function format a model value to be presented in the input field.
			function formatter(modelValue) {
				//console.log("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
				//console.log("NUMBER FORMATTER");
				//console.log("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
				//console.log(modelValue);

				if(!_eventsBound) {return modelValue;}

				// Update the unmasked value.
				return mask(modelValue);
			}

			// This function parse the value presented in the input field to populate the model.
			function parser(viewValue) {
				//console.log("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
				//console.log("NUMBER PARSER");
				//console.log("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
				//console.log(viewValue);

				if(!_eventsBound) {return viewValue;}

				return unmask(viewValue);
			}

			// Push formatters and parsers
			ctrl.$formatters.push(formatter);
			ctrl.$parsers.push(parser);

			// Watch changes
			attrs.$observe("gNumber", init);

			// Initialize
			init(attrs.gNumber);
		}
	};
}]);
/**
 * Taken from angular ui bootstrap project.
 * angular-ui.github.io/bootstrap/
 */

angular.module('grass.modal', ['grass.utils.transition'])

//##############################################################
//	Stacked Map Service
//	
//	A helper, internal data structure that acts as a map but also 
//	allows getting / removing elements in the LIFO order
//##############################################################
.factory('$$stackedMap', function () {
	return {
		createNew: function () {
			var stack = [];

			return {
				add: function (key, value) {
					stack.push({
						key: key,
						value: value
					});
				},
				get: function (key) {
					for (var i = 0; i < stack.length; i++) {
						if (key == stack[i].key) {
							return stack[i];
						}
					}
				},
				keys: function() {
					var keys = [];
					for (var i = 0; i < stack.length; i++) {
						keys.push(stack[i].key);
					}
					return keys;
				},
				top: function () {
					return stack[stack.length - 1];
				},
				remove: function (key) {
					var idx = -1;
					for (var i = 0; i < stack.length; i++) {
						if (key == stack[i].key) {
							idx = i;
							break;
						}
					}
					return stack.splice(idx, 1)[0];
				},
				removeTop: function () {
					return stack.splice(stack.length - 1, 1)[0];
				},
				length: function () {
					return stack.length;
				}
			};
		}
	};
})

//##############################################################
//	Modal Stack Service
//##############################################################
.factory('$modalStack', ['$transition', '$timeout', '$document', '$compile', '$rootScope', '$$stackedMap',
function ($transition, $timeout, $document, $compile, $rootScope, $$stackedMap) {

	var OPENED_MODAL_CLASS = 'modal-open';

	var backdropDomEl, backdropScope;
	var openedWindows = $$stackedMap.createNew();
	var $modalStack = {};

	function backdropIndex() {
		var topBackdropIndex = -1;
		var opened = openedWindows.keys();
		for (var i = 0; i < opened.length; i++) {
			if (openedWindows.get(opened[i]).value.backdrop) {
				topBackdropIndex = i;
			}
		}
		return topBackdropIndex;
	}

	$rootScope.$watch(backdropIndex, function(newBackdropIndex){
		if (backdropScope) {
			backdropScope.index = newBackdropIndex;
		}
	});

	function removeModalWindow(modalInstance) {

		var body = $document.find('body').eq(0);
		var modalWindow = openedWindows.get(modalInstance).value;

		//clean up the stack
		openedWindows.remove(modalInstance);

		//remove window DOM element
		removeAfterAnimate(modalWindow.modalDomEl, modalWindow.modalScope, 300, checkRemoveBackdrop);
		body.toggleClass(OPENED_MODAL_CLASS, openedWindows.length() > 0);
	}

	function checkRemoveBackdrop() {
			//remove backdrop if no longer needed
			if (backdropDomEl && backdropIndex() == -1) {
				var backdropScopeRef = backdropScope;
				removeAfterAnimate(backdropDomEl, backdropScope, 150, function () {
					backdropScopeRef.$destroy();
					backdropScopeRef = null;
				});
				backdropDomEl = undefined;
				backdropScope = undefined;
			}
	}

	function removeAfterAnimate(domEl, scope, emulateTime, done) {
		// Closing animation
		scope.animate = false;

		var transitionEndEventName = $transition.transitionEndEventName;
		if (transitionEndEventName) {
			// transition out
			var timeout = $timeout(afterAnimating, emulateTime);

			domEl.bind(transitionEndEventName, function () {
				$timeout.cancel(timeout);
				afterAnimating();
				scope.$apply();
			});
		} else {
			// Ensure this call is async
			$timeout(afterAnimating, 0);
		}

		function afterAnimating() {
			if (afterAnimating.done) {
				return;
			}
			afterAnimating.done = true;

			domEl.remove();
			if (done) {
				done();
			}
		}
	}

	$document.bind('keydown', function (evt) {
		var modal;

		if (evt.which === 27) {
			modal = openedWindows.top();
			if (modal && modal.value.keyboard) {
				$rootScope.$apply(function () {
					$modalStack.dismiss(modal.key);
				});
			}
		}
	});

	$modalStack.open = function (modalInstance, modal) {

		openedWindows.add(modalInstance, {
			deferred: modal.deferred,
			modalScope: modal.scope,
			backdrop: modal.backdrop,
			keyboard: modal.keyboard
		});

		var body = $document.find('body').eq(0),
				currBackdropIndex = backdropIndex();

		if (currBackdropIndex >= 0 && !backdropDomEl) {
			backdropScope = $rootScope.$new(true);
			backdropScope.index = currBackdropIndex;
			backdropDomEl = $compile('<div modal-backdrop></div>')(backdropScope);
			body.append(backdropDomEl);
		}
			
		var angularDomEl = angular.element('<div modal-window></div>');
		angularDomEl.attr('window-class', modal.windowClass);
		angularDomEl.attr('index', openedWindows.length() - 1);
		angularDomEl.attr('animate', 'animate');
		angularDomEl.html(modal.content);

		var modalDomEl = $compile(angularDomEl)(modal.scope);
		openedWindows.top().value.modalDomEl = modalDomEl;
		body.append(modalDomEl);
		body.addClass(OPENED_MODAL_CLASS);
	};

	$modalStack.close = function (modalInstance, result) {
		var modalWindow = openedWindows.get(modalInstance).value;
		if (modalWindow) {
			modalWindow.deferred.resolve(result);
			removeModalWindow(modalInstance);
		}
	};

	$modalStack.dismiss = function (modalInstance, reason) {
		var modalWindow = openedWindows.get(modalInstance).value;
		if (modalWindow) {
			modalWindow.deferred.reject(reason);
			removeModalWindow(modalInstance);
		}
	};

	$modalStack.dismissAll = function (reason) {
		var topModal = this.getTop();
		while (topModal) {
			this.dismiss(topModal.key, reason);
			topModal = this.getTop();
		}
	};

	$modalStack.getTop = function () {
		return openedWindows.top();
	};

	return $modalStack;
}])

//##############################################################
//	Modal Main Service
//##############################################################
.provider('$modal', function () {

	var $modalProvider = {
		options: {
			backdrop: true, //can be also false or 'static'
			keyboard: true
		},
		$get: ['$injector', '$rootScope', '$q', '$http', '$templateCache', '$controller', '$modalStack',
			function ($injector, $rootScope, $q, $http, $templateCache, $controller, $modalStack) {

				var $modal = {};

				function getTemplatePromise(options) {
					return options.template ? $q.when(options.template) :
						$http.get(options.templateUrl, {cache: $templateCache}).then(function (result) {
							return result.data;
						});
				}

				function getResolvePromises(resolves) {
					var promisesArr = [];
					angular.forEach(resolves, function (value, key) {
						if (angular.isFunction(value) || angular.isArray(value)) {
							promisesArr.push($q.when($injector.invoke(value)));
						}
					});
					return promisesArr;
				}

				$modal.open = function (modalOptions) {

					var modalResultDeferred = $q.defer();
					var modalOpenedDeferred = $q.defer();

					//prepare an instance of a modal to be injected into controllers and returned to a caller
					var modalInstance = {
						result: modalResultDeferred.promise,
						opened: modalOpenedDeferred.promise,
						close: function (result) {
							$modalStack.close(modalInstance, result);
						},
						dismiss: function (reason) {
							$modalStack.dismiss(modalInstance, reason);
						}
					};

					//merge and clean up options
					modalOptions = angular.extend({}, $modalProvider.options, modalOptions);
					modalOptions.resolve = modalOptions.resolve || {};

					//verify options
					if (!modalOptions.template && !modalOptions.templateUrl) {
						throw new Error('One of template or templateUrl options is required.');
					}

					var templateAndResolvePromise =
						$q.all([getTemplatePromise(modalOptions)].concat(getResolvePromises(modalOptions.resolve)));


					templateAndResolvePromise.then(function resolveSuccess(tplAndVars) {

						var modalScope = (modalOptions.scope || $rootScope).$new();
						modalScope.$close = modalInstance.close;
						modalScope.$dismiss = modalInstance.dismiss;

						var ctrlInstance, ctrlLocals = {};
						var resolveIter = 1;

						//controllers
						if (modalOptions.controller) {
							ctrlLocals.$scope = modalScope;
							ctrlLocals.$modalInstance = modalInstance;
							angular.forEach(modalOptions.resolve, function (value, key) {
								ctrlLocals[key] = tplAndVars[resolveIter++];
							});

							ctrlInstance = $controller(modalOptions.controller, ctrlLocals);
						}

						$modalStack.open(modalInstance, {
							scope: modalScope,
							deferred: modalResultDeferred,
							content: tplAndVars[0],
							backdrop: modalOptions.backdrop,
							keyboard: modalOptions.keyboard,
							windowClass: modalOptions.windowClass
						});

					}, function resolveError(reason) {
						modalResultDeferred.reject(reason);
					});

					templateAndResolvePromise.then(function () {
						modalOpenedDeferred.resolve(true);
					}, function () {
						modalOpenedDeferred.reject(false);
					});

					return modalInstance;
				};

				return $modal;
			}]
	};

	return $modalProvider;
})

//##############################################################
//	Modal Backdrop Directive
//##############################################################
.directive('modalBackdrop', ['$timeout', function($timeout) {
	return {
		restrict: 'EA',
		replace: true,
		templateUrl: 'templates/modal/backdrop.html',
		link: function(scope) {
			scope.animate = false;

			$timeout(function() {
				scope.animate = true; // Trigger CSS transitions.
			});
		}
	};
}])

//##############################################################
//	Modal Window Directive
//##############################################################
.directive('modalWindow', ['$timeout', '$modalStack', function($timeout, $modalStack) {
	return {
		restrict: 'EA',
		replace: true,
		transclude: true,
		templateUrl: 'templates/modal/window.html',
		scope: {
			index: '@',
			animate: '='
		},
		link: function(scope, elem, attrs) {
			scope.windowClass = attrs.windowClass || "";

			$timeout(function() {
				scope.animate = true;	// Trigger CSS transitions.
				elem[0].focus();		// Focus the new modal.
			});

			scope.close = function(evt) {
				var modal = $modalStack.getTop();

				if(	modal && 
					modal.value.backdrop && 
					modal.value.backdrop != 'static' &&
					evt.target === evt.currentTarget) {

					evt.preventDefault();
					evt.stopPropagation();
					$modalStack.dismiss(modal.key, 'backdrop click');
				}
			};
		}
	};
}]);
angular.module('grass.notifier', [])

.service('gNotifier', ['$rootScope', function($rootScope) {
	return {
		post: function(type, message) {
			console.log('posting notification!');

			this.type = type;
			this.message = message;

			$rootScope.$broadcast('g:notifier:post');
		},

		clear: function() {
			$rootScope.$broadcast('g:notifier:clear');	
		}
	};
}])

.directive('gNotificationCenter', ['$timeout', 'gNotifier', function($timeout, gNotifier) {
	return {
		replace: true,
		restrict: 'EA',
		template: 
			'<div class="notification-center">' +
				'<div class="box shadow notification" ng-class="notification.type" ng-repeat="notification in notifications">' +
					'<div class="box-header" ng-if="notification.title">' +
						'<div class="notification-title">{{notification.title}}</div>' +
					'</div>' +
					'<div class="box-body">' +
						'{{notification.message}}' +
					'</div>' +
				'</div>' +
			'</div>',

		link: function(scope, elem, attrs, ctrl) {
			scope.notifications = [];

			var Notification = function(type, message) {
				this.id = scope.notifications.length;
				this.type = 'notification-' + type;
				this.message = message;
				this.alreadyPushed = false;

				this.post = function() {
					if(this.alreadyPushed){return;}
					scope.notifications.unshift(this);
					this.alreadyPushed = true;
				};

				this.setTimeout = function() {
					var self = this;
					this.timeout = $timeout(function() {
						self.remove();
					}, 3000);
				};

				this.remove = function() {
					var idx = scope.notifications.indexOf(this);
					if(idx > -1) {scope.notifications.splice(idx, 1);}
				};
			};

			scope.$on('g:notifier:post', function() {
				console.log('notification posted');
				console.log('type = ' + gNotifier.type);
				console.log('message = ' + gNotifier.message);

				var notification = new Notification(gNotifier.type, gNotifier.message);
				notification.post();
				notification.setTimeout();
			});
		}
	};
}]);
/**
 * Taken from angular ui bootstrap project.
 * angular-ui.github.io/bootstrap/
 */

angular.module('grass.progressbar', [])

.constant('progressConfig', {
	animate: true,
	max: 100
})

.controller('ProgressController', ['$scope', '$attrs', 'progressConfig', function($scope, $attrs, progressConfig) {
	var self = this,
		animate = angular.isDefined($attrs.animate) ? $scope.$parent.$eval($attrs.animate) : progressConfig.animate;

	this.bars = [];
	$scope.max = angular.isDefined($attrs.max) ? $scope.$parent.$eval($attrs.max) : progressConfig.max;

	this.addBar = function(bar, element) {
		if ( !animate ) {
			element.css({'transition': 'none'});
		}

		this.bars.push(bar);

		bar.$watch('value', function( value ) {
			bar.percent = +(100 * value / $scope.max).toFixed(2);
		});

		bar.$on('$destroy', function() {
			element = null;
			self.removeBar(bar);
		});
	};

	this.removeBar = function(bar) {
		this.bars.splice(this.bars.indexOf(bar), 1);
	};
}])

.directive('progress', function() {
	return {
		restrict: 'EA',
		replace: true,
		transclude: true,
		controller: 'ProgressController',
		require: 'progress',
		scope: {},
		templateUrl: 'templates/progressbar/progress.html'
	};
})

.directive('bar', function() {
	return {
		restrict: 'EA',
		replace: true,
		transclude: true,
		require: '^progress',
		scope: {
			value: '=',
			type: '@'
		},
		templateUrl: 'templates/progressbar/bar.html',
		link: function(scope, element, attrs, progressCtrl) {
			progressCtrl.addBar(scope, element);
		}
	};
})

.directive('progressbar', function() {
	return {
		restrict: 'EA',
		replace: true,
		transclude: true,
		controller: 'ProgressController',
		scope: {
			value: '=',
			type: '@'
		},
		templateUrl: 'templates/progressbar/progressbar.html',
		link: function(scope, element, attrs, progressCtrl) {
			progressCtrl.addBar(scope, angular.element(element.children()[0]));
		}
	};
});
angular.module("templates/modal/backdrop.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("templates/modal/backdrop.html",
    "<div class=\"modal-backdrop\" ng-class=\"{in: animate}\" ng-style=\"{'z-index': 1040 + index*10}\"></div>");
}]);

angular.module("templates/modal/window.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("templates/modal/window.html",
    "<div tabindex=\"-1\" class=\"modal {{ windowClass }}\" ng-class=\"{in: animate}\" ng-style=\"{'z-index': 1050 + index*10, display: 'block'}\" ng-click=\"close($event)\">\n" +
    "    <div class=\"modal-dialog\"><div class=\"modal-content\" ng-transclude></div></div>\n" +
    "</div>");
}]);

angular.module("templates/tooltip/tooltip-html-unsafe-popup.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("templates/tooltip/tooltip-html-unsafe-popup.html",
    "<div class=\"tooltip {{placement}}\" ng-class=\"{ in: isOpen(), fade: animation() }\">\n" +
    "  <div class=\"tooltip-arrow\"></div>\n" +
    "  <div class=\"tooltip-inner\" bind-html-unsafe=\"content\"></div>\n" +
    "</div>\n" +
    "");
}]);

angular.module("templates/tooltip/tooltip-popup.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("templates/tooltip/tooltip-popup.html",
    "<div class=\"tooltip {{placement}}\" ng-class=\"{ in: isOpen(), fade: animation() }\">\n" +
    "  <div class=\"tooltip-arrow\"></div>\n" +
    "  <div class=\"tooltip-inner\" ng-bind=\"content\"></div>\n" +
    "</div>\n" +
    "");
}]);

angular.module("templates/progressbar/progress.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("templates/progressbar/progress.html",
    "<div class=\"progress\" ng-transclude></div>\n" +
    "");
}]);

angular.module("templates/progressbar/bar.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("templates/progressbar/bar.html",
    "<div class=\"progress-bar\" ng-class=\"type && 'progress-bar-' + type\" role=\"progressbar\" aria-valuenow=\"{{value}}\" aria-valuemin=\"0\" aria-valuemax=\"{{max}}\" ng-style=\"{width: percent + '%'}\" aria-valuetext=\"{{percent | number:0}}%\" ng-transclude></div>\n" +
    "");
}]);

angular.module("templates/progressbar/progressbar.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("templates/progressbar/progressbar.html",
    "<div class=\"progress\">\n" +
      "<div class=\"progress-bar\" ng-class=\"type && 'progress-bar-' + type\" role=\"progressbar\" aria-valuenow=\"{{value}}\" aria-valuemin=\"0\" aria-valuemax=\"{{max}}\" ng-style=\"{width: percent + '%'}\" aria-valuetext=\"{{percent | number:0}}%\" ng-transclude></div>\n" +
    "</div>\n" + 
    "");
}]);








/**
 * Taken from angular ui bootstrap project.
 * angular-ui.github.io/bootstrap/
 */

angular.module( 'grass.tooltip', [ 'grass.utils.position', 'grass.utils.bind' ] )

/**
 * The $tooltip service creates tooltip- and popover-like directives as well as
 * houses global options for them.
 */
.provider( '$tooltip', function () {
	// The default options tooltip and popover.
	var defaultOptions = {
		placement: 'bottom',
		animation: true,
		popupDelay: 0
	};

	// Default hide triggers for each show trigger
	var triggerMap = {
		'mouseenter': 'mouseleave',
		'click': 'click',
		'focus': 'blur'
	};

	// The options specified to the provider globally.
	var globalOptions = {};
	
	/**
	 * `options({})` allows global configuration of all tooltips in the
	 * application.
	 *
	 *   var app = angular.module( 'App', ['ui.bootstrap.tooltip'], function( $tooltipProvider ) {
	 *     // place tooltips left instead of top by default
	 *     $tooltipProvider.options( { placement: 'left' } );
	 *   });
	 */
	this.options = function( value ) {
		angular.extend( globalOptions, value );
	};

	/**
	 * This allows you to extend the set of trigger mappings available. E.g.:
	 *
	 *   $tooltipProvider.setTriggers( 'openTrigger': 'closeTrigger' );
	 */
	this.setTriggers = function setTriggers ( triggers ) {
		angular.extend( triggerMap, triggers );
	};

	/**
	 * This is a helper function for translating camel-case to snake-case.
	 */
	function snake_case(name){
		var regexp = /[A-Z]/g;
		var separator = '-';
		return name.replace(regexp, function(letter, pos) {
			return (pos ? separator : '') + letter.toLowerCase();
		});
	}

	/**
	 * Returns the actual instance of the $tooltip service.
	 * TODO support multiple triggers
	 */
	this.$get = [ '$window', '$compile', '$timeout', '$parse', '$document', '$position', '$interpolate', function ( $window, $compile, $timeout, $parse, $document, $position, $interpolate ) {
		return function $tooltip ( type, prefix, defaultTriggerShow ) {
			var options = angular.extend( {}, defaultOptions, globalOptions );

			/**
			 * Returns an object of show and hide triggers.
			 *
			 * If a trigger is supplied,
			 * it is used to show the tooltip; otherwise, it will use the `trigger`
			 * option passed to the `$tooltipProvider.options` method; else it will
			 * default to the trigger supplied to this directive factory.
			 *
			 * The hide trigger is based on the show trigger. If the `trigger` option
			 * was passed to the `$tooltipProvider.options` method, it will use the
			 * mapped trigger from `triggerMap` or the passed trigger if the map is
			 * undefined; otherwise, it uses the `triggerMap` value of the show
			 * trigger; else it will just use the show trigger.
			 */
			function getTriggers ( trigger ) {
				var show = trigger || options.trigger || defaultTriggerShow;
				var hide = triggerMap[show] || show;
				return {
					show: show,
					hide: hide
				};
			}

			var directiveName = snake_case( type );

			var startSym = $interpolate.startSymbol();
			var endSym = $interpolate.endSymbol();
			var template = 
				'<div '+ directiveName +'-popup '+
					'title="'+startSym+'tt_title'+endSym+'" '+
					'content="'+startSym+'tt_content'+endSym+'" '+
					'placement="'+startSym+'tt_placement'+endSym+'" '+
					'animation="tt_animation" '+
					'is-open="tt_isOpen"'+
					'>'+
				'</div>';

			return {
				restrict: 'EA',
				scope: true,
				compile: function (tElem, tAttrs) {
					var tooltipLinker = $compile( template );

					return function link ( scope, element, attrs ) {
						var tooltip;
						var transitionTimeout;
						var popupTimeout;
						var appendToBody = angular.isDefined( options.appendToBody ) ? options.appendToBody : false;
						var triggers = getTriggers( undefined );
						var hasRegisteredTriggers = false;
						var hasEnableExp = angular.isDefined(attrs[prefix+'Enable']);

						var positionTooltip = function (){
							var position,
								ttWidth,
								ttHeight,
								ttPosition;
							// Get the position of the directive element.
							position = appendToBody ? $position.offset( element ) : $position.position( element );

							// Get the height and width of the tooltip so we can center it.
							ttWidth = tooltip.prop( 'offsetWidth' );
							ttHeight = tooltip.prop( 'offsetHeight' );

							// Calculate the tooltip's top and left coordinates to center it with
							// this directive.
							switch ( scope.tt_placement ) {
								case 'right':
									ttPosition = {
										top: position.top + position.height / 2 - ttHeight / 2,
										left: position.left + position.width
									};
									break;
								case 'bottom':
									ttPosition = {
										top: position.top + position.height,
										left: position.left + position.width / 2 - ttWidth / 2
									};
									break;
								case 'left':
									ttPosition = {
										top: position.top + position.height / 2 - ttHeight / 2,
										left: position.left - ttWidth
									};
									break;
								default:
									ttPosition = {
										top: position.top - ttHeight,
										left: position.left + position.width / 2 - ttWidth / 2
									};
									break;
							}

							ttPosition.top += 'px';
							ttPosition.left += 'px';

							// Now set the calculated positioning.
							tooltip.css( ttPosition );

						};

						// By default, the tooltip is not open.
						// TODO add ability to start tooltip opened
						scope.tt_isOpen = false;

						function toggleTooltipBind () {
							if ( ! scope.tt_isOpen ) {
								showTooltipBind();
							} else {
								hideTooltipBind();
							}
						}

						// Show the tooltip with delay if specified, otherwise show it immediately
						function showTooltipBind() {
							if(hasEnableExp && !scope.$eval(attrs[prefix+'Enable'])) {
								return;
							}
							if ( scope.tt_popupDelay ) {
								popupTimeout = $timeout( show, scope.tt_popupDelay, false );
								popupTimeout.then(function(reposition){reposition();});
							} else {
								show()();
							}
						}

						function hideTooltipBind () {
							scope.$apply(function () {
								hide();
							});
						}

						// Show the tooltip popup element.
						function show() {


							// Don't show empty tooltips.
							if ( ! scope.tt_content ) {
								return angular.noop;
							}

							createTooltip();

							// If there is a pending remove transition, we must cancel it, lest the
							// tooltip be mysteriously removed.
							if ( transitionTimeout ) {
								$timeout.cancel( transitionTimeout );
							}

							// Set the initial positioning.
							tooltip.css({ top: 0, left: 0, display: 'block' });

							// Now we add it to the DOM because need some info about it. But it's not 
							// visible yet anyway.
							if ( appendToBody ) {
									$document.find( 'body' ).append( tooltip );
							} else {
								element.after( tooltip );
							}

							positionTooltip();

							// And show the tooltip.
							scope.tt_isOpen = true;
							scope.$digest(); // digest required as $apply is not called

							// Return positioning function as promise callback for correct
							// positioning after draw.
							return positionTooltip;
						}

						// Hide the tooltip popup element.
						function hide() {
							// First things first: we don't show it anymore.
							scope.tt_isOpen = false;

							//if tooltip is going to be shown after delay, we must cancel this
							$timeout.cancel( popupTimeout );

							// And now we remove it from the DOM. However, if we have animation, we 
							// need to wait for it to expire beforehand.
							// FIXME: this is a placeholder for a port of the transitions library.
							if ( scope.tt_animation ) {
								transitionTimeout = $timeout(removeTooltip, 500);
							} else {
								removeTooltip();
							}
						}

						function createTooltip() {
							// There can only be one tooltip element per directive shown at once.
							if (tooltip) {
								removeTooltip();
							}
							tooltip = tooltipLinker(scope, function () {});

							// Get contents rendered into the tooltip
							scope.$digest();
						}

						function removeTooltip() {
							if (tooltip) {
								tooltip.remove();
								tooltip = null;
							}
						}

						/**
						 * Observe the relevant attributes.
						 */
						attrs.$observe( type, function ( val ) {
							scope.tt_content = val;

							if (!val && scope.tt_isOpen ) {
								hide();
							}
						});

						attrs.$observe( prefix+'Title', function ( val ) {
							scope.tt_title = val;
						});

						attrs.$observe( prefix+'Placement', function ( val ) {
							scope.tt_placement = angular.isDefined( val ) ? val : options.placement;
						});

						attrs.$observe( prefix+'PopupDelay', function ( val ) {
							var delay = parseInt( val, 10 );
							scope.tt_popupDelay = ! isNaN(delay) ? delay : options.popupDelay;
						});

						var unregisterTriggers = function() {
							if (hasRegisteredTriggers) {
								element.unbind( triggers.show, showTooltipBind );
								element.unbind( triggers.hide, hideTooltipBind );
							}
						};

						attrs.$observe( prefix+'Trigger', function ( val ) {
							unregisterTriggers();

							triggers = getTriggers( val );

							if ( triggers.show === triggers.hide ) {
								element.bind( triggers.show, toggleTooltipBind );
							} else {
								element.bind( triggers.show, showTooltipBind );
								element.bind( triggers.hide, hideTooltipBind );
							}

							hasRegisteredTriggers = true;
						});

						var animation = scope.$eval(attrs[prefix + 'Animation']);
						scope.tt_animation = angular.isDefined(animation) ? !!animation : options.animation;

						attrs.$observe( prefix+'AppendToBody', function ( val ) {
							appendToBody = angular.isDefined( val ) ? $parse( val )( scope ) : appendToBody;
						});

						// if a tooltip is attached to <body> we need to remove it on
						// location change as its parent scope will probably not be destroyed
						// by the change.
						if ( appendToBody ) {
							scope.$on('$locationChangeSuccess', function closeTooltipOnLocationChangeSuccess () {
							if ( scope.tt_isOpen ) {
								hide();
							}
						});
						}

						// Make sure tooltip is destroyed and removed.
						scope.$on('$destroy', function onDestroyTooltip() {
							$timeout.cancel( transitionTimeout );
							$timeout.cancel( popupTimeout );
							unregisterTriggers();
							removeTooltip();
						});
					};
				}
			};
		};
	}];
})

.directive( 'tooltipPopup', function () {
	return {
		restrict: 'EA',
		replace: true,
		scope: { content: '@', placement: '@', animation: '&', isOpen: '&' },
		templateUrl: 'templates/tooltip/tooltip-popup.html'
	};
})

.directive( 'tooltip', [ '$tooltip', function ( $tooltip ) {
	return $tooltip( 'tooltip', 'tooltip', 'mouseenter' );
}])

.directive( 'tooltipHtmlUnsafePopup', function () {
	return {
		restrict: 'EA',
		replace: true,
		scope: { content: '@', placement: '@', animation: '&', isOpen: '&' },
		templateUrl: 'templates/tooltip/tooltip-html-unsafe-popup.html'
	};
})

.directive( 'tooltipHtmlUnsafe', [ '$tooltip', function ( $tooltip ) {
	return $tooltip( 'tooltipHtmlUnsafe', 'tooltip', 'mouseenter' );
}]);
/**
 * Taken from angular ui bootstrap project.
 * angular-ui.github.io/bootstrap/
 */
angular.module('grass.utils.bind', [])

.directive('bindHtmlUnsafe', function () {
	return function (scope, element, attr) {
		element.addClass('ng-binding').data('$binding', attr.bindHtmlUnsafe);
		scope.$watch(attr.bindHtmlUnsafe, function bindHtmlUnsafeWatchAction(value) {
			element.html(value || '');
		});
	};
});
angular.module('grass.utils.graph', [

])

//##############################################################
// Directed Graph
//##############################################################
.factory('gDirectedGraph', function() {

	var gDirectedGraph = function(numVertices) {
		var _adj = [];
		var _numEdges = 0;
		var _numVertices = numVertices || 0;

		//-----------------------------------------------------
		//	Instance Functions
		//-----------------------------------------------------
		this.addEdge = function(vIdx, uIdx) {
			_adj[vIdx].push(uIdx);
			_numEdges++;
		};

		this.numVertices = function() {
			return _numVertices;
		};

		this.adj = function(vIdx) {
			return (vIdx >= 0 && vIdx < _numVertices ? _adj[vIdx] : []);
		};

		this.print = function() {
			console.log("############################");
			console.log("# Printing Graph");
			console.log("############################");
			console.log("|    numVertices = " + _numVertices);
			console.log("|    numEdges = " + _numEdges);

			for(var i = 0; i < _numVertices; i++) {
				console.log("|        v[" + i + "] = " + _adj[i].join(", "));
			}
		};

		//-----------------------------------------------------
		//	Initialization
		//-----------------------------------------------------
		for(var i = 0; i < numVertices; i++) {
			_adj.push([]);
		}
	};

	return gDirectedGraph;
})

.factory('gDirectedDFS', function() {
	var gDirectedDFS = function(graph, sources) {
		console.log("############################");
		console.log("# Direct DFS");
		console.log("############################");

		var _graph = graph;
		var _sources = (sources instanceof Array ? sources : [sources]);
		var _marked = [];

		console.log("|    sources = " + _sources.join(", "));
		console.log("");
		
		//-----------------------------------------------------
		//	Private Functions
		//-----------------------------------------------------
		var _dfs = function(vIdx) {
			var adj = _graph.adj(vIdx);

			console.log("|        dfs for " + vIdx);
			console.log("|            Adjs = " + adj.join(", "));

			_marked[vIdx] = true;

			for( var i = 0; i < adj.length; i++ ) {
				if(!_marked[adj[i]]) {_dfs(adj[i]);}
			}
		};

		//-----------------------------------------------------
		//	Instance Functions
		//-----------------------------------------------------
		this.marked = function(vIdx) {
			return (vIdx >= 0 && vIdx < _graph.numVertices() ? _marked[vIdx] : undefined);
		};

		
		//-----------------------------------------------------
		//	Initialization
		//-----------------------------------------------------
		// Initializing _marked array.
		for(var i = 0; i < _graph.numVertices(); i++) {
			_marked.push(false);
		}

		// Search
		for(i = 0; i < _sources.length; i++) {
			console.log("|    src " + i + ":");

			if(!_marked[_sources[i]]) {
				console.log("|        Not Marked!");
				_dfs(_sources[i]);
			}
		}
	};

	return gDirectedDFS;
});
angular.module('grass.utils.input', [
])

.value('gInputUtils', {
	ignoreKeys: [
		16, // Shift
		17,	// Ctrl
		18,	// Alt
		20, // Caps Lock
		37,	// Left Arrow
		38,	// Up Arrow
		39,	// Right Arrow
		40,	// Down Arrow
		91,	// Left Window
		92, // Right Window

	],

	grammar: {
		'1': { definition: /[1-9]/ },
		'9': { definition: /[0-9]/ },
		'a': { definition: /[a-zA-Z]/},
		'#': { definition: /[a-zA-Z0-9]/}
	}
});
angular.module('grass.utils.masker', [])

.factory('gMaskerUtils', function() {
	return {
		TAGS: {
			pattern: 'pattern',
			symbol: 'symbol'
		},

		options: {
			ignoreKeys: [16,17,18,37,38,39,40,91]
		},

		findGrammarPattern: function(chr, grammar) {
			for (var key in grammar) {
				if (grammar.hasOwnProperty(key) && chr == key) {
					return grammar[key];
				}
			}

			return false;
		},

		tokenize: function(expression, grammar) {
			var tokens = [];

			for(var i = 0; i < expression.length; i++) {
				var chr = expression[i];
				var value = null;

				if(value = this.findGrammarPattern(chr, grammar)) {
					tokens.push({
						tag: this.TAGS.pattern,
						pattern: value
					});
				}
				else {
					tokens.push({
						tag: this.TAGS.symbol,
						symbol: chr
					});
				}
			}

			return tokens;
		}
	};
})

.factory('gMasker', ['gMaskerUtils', function(gMaskerUtils) {
	var gMasker = function(expression, grammar, skipSymbols) {
		var _grammar = grammar;
		var _skipSymbols = (skipSymbols || []);
		var _tokens = gMaskerUtils.tokenize(expression, grammar);

		console.log(_tokens);

		this.mask = function(text) {
			if(!text) {return "";}

			console.log("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
			console.log("Masking");
			console.log("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");

			var i = 0, j = 0, masked = "";

			while(i < text.length && j < _tokens.length) {
				var chr = text[i];

				if( (_tokens[j].tag == gMaskerUtils.TAGS.pattern && chr.match(_tokens[j].pattern.definition)) ||
					(_tokens[j].tag == gMaskerUtils.TAGS.symbol && chr == _tokens[j].symbol)	) {
					masked += chr;
					i++;
				}
				else if( _tokens[j].tag == gMaskerUtils.TAGS.symbol ) {
					masked += _tokens[j].symbol; // Não move texto. Move pattern.
				}
				else {
					return masked;
				}

				j++;
			}

			return masked;
		};

		this.unmask = function(text) {
			if(!text) {return "";}

			var i = 0, j = 0, unmasked = "";

			console.log("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
			console.log("UnMasking");
			console.log("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");

			// Remove symbols
			while(i < text.length && j < _tokens.length) {
				var chr = text[i];

				console.log("    text[" + i + "] = " + chr);

				if(_tokens[j].tag != gMaskerUtils.TAGS.symbol || _skipSymbols.indexOf(chr) >= 0 ) {
					console.log("        pattern || skiped");
					unmasked += chr;
				}

				console.log("        unmasked = " + unmasked);

				i++; j++;
			}

			return unmasked;
		};

		this.setSkipSymbols = function(skipSymbols) {
			if(typeof skipSymbols == 'string') { skipSymbols = [skipSymbols];}

			_skipSymbols = skipSymbols || [];
		};
	};

	return gMasker;
}]);
angular.module('grass.utils.matcher', [
	'grass.utils.graph'
])

.factory('gMatcherUtils', function() {
	return {
		TAGS: {
			start:		0,
			end:		1,
			closure:	2,
			optional:	3,
			or:			4,
			pattern:	5,
			symbol:		6
		},

		findGrammarPattern: function(chr, grammar) {
			var pattern = false;

			angular.forEach(grammar.patterns, function(value, key) {
				if(chr == key) {
					pattern = value;
				}
			});

			return pattern;
		},

		tokenize: function(expression, grammar, symbols) {
			console.log("|    Tokenizing:");
			var tokens = [];

			for(var i = 0; i < expression.length; i++) {
				var chr = expression[i];
				var value = null;

				console.log("|        chr = " + chr);

				if(chr == grammar.group.start) {
					console.log("|            Start");
					tokens.push({
						tag: this.TAGS.start
					});
				}
				else if(chr == grammar.group.end) {
					console.log("|            End");
					tokens.push({
						tag: this.TAGS.end
					});
				}
				else if(chr == grammar.closure) {
					console.log("|            Closure");
					tokens.push({
						tag: this.TAGS.closure
					});
				}
				else if(chr == grammar.optional) {
					console.log("|            Optional");
					tokens.push({
						tag: this.TAGS.optional
					});
				}
				else if(chr == grammar.or) {
					console.log("|            Or");
					tokens.push({
						tag: this.TAGS.or
					});
				}
				else if(value = this.findGrammarPattern(chr, grammar)) {
					console.log("|            Pattern");
					tokens.push({
						tag: this.TAGS.pattern,
						value: value
					});
				}
				else {
					console.log("|            Symbol");

					if(symbols) {symbols.push(chr);}
					
					tokens.push({
						tag: this.TAGS.symbol,
						value: chr
					});
				}
			}

			return tokens;
		},

		parse: function(tokens) {
			var graph = new DirectedGraph(tokens.length+1);
			var stack = [];

			for(var i = 0; i < tokens.length; i++) {
				var lp = i;

				if(tokens[i].tag == this.TAGS.start || 
					tokens[i].tag == this.TAGS.or) {
					stack.push(i);
				}
				else if(tokens[i].tag == this.TAGS.end) {
					var or = stack.pop();

					if(tokens[or].tag == this.TAGS.or) {
						lp = stack.pop();
						graph.addEdge(lp, or+1);
						graph.addEdge(or,i);
					}
					else { lp = or; }
				}

				// Lookahead
				if(i < (tokens.length - 1)) {
					if(tokens[i+1].tag == this.TAGS.closure) {
						graph.addEdge(lp, i+1);
						graph.addEdge(i+1, lp);
					}
					else if(tokens[i+1].tag == this.TAGS.optional) {
						graph.addEdge(lp, i+1);
					}
				}

				if( tokens[i].tag == this.TAGS.start || 
					tokens[i].tag == this.TAGS.closure || 
					tokens[i].tag == this.TAGS.optional || 
					tokens[i].tag == this.TAGS.end ) { graph.addEdge(i, i+1); }
			}

			return graph;
		},

		parseReverse: function(tokens) {
			console.log("REVERSE PARSING!");

			var graph = new DirectedGraph(tokens.length+1);
			var stack = [];
			var rev_stack = [];

			for(var i = tokens.length; i > 0; i--) {
				console.log('node = ' + i);

				var rp = i;

				if( tokens[i-1].tag == this.TAGS.end && (i-1) < tokens.length-1 && (tokens[i].tag == this.TAGS.closure || tokens[i].tag == this.TAGS.optional) ) {
					console.log('    end + clousure/optional');

					stack.push(i);
					stack.push(i+1);

					console.log('        stack = ' + stack.join(', '));
				}
				else if( tokens[i-1].tag == this.TAGS.end || tokens[i-1].tag == this.TAGS.or ) {
					console.log('    end/or');

					stack.push(i);

					console.log('        stack = ' + stack.join(', '));
				}
				else if(tokens[i-1].tag == this.TAGS.start) {
					console.log('    start');
					console.log('        stack = ' + stack.join(', '));

					var or = false;
					var op = stack.pop();

					if(tokens[op-1].tag == this.TAGS.or) {
						console.log('            op = or');
						console.log('                edge = (' + op + ', ' + i + ')');

						graph.addEdge(op, i);
						or = op;
						op = stack.pop();

						console.log('        stack = ' + stack.join(', '));
					}
					
					if(tokens[op-1].tag == this.TAGS.closure) {
						console.log('            op = closure');
						console.log('                edge = (' + op + ', ' + i + ')');
						console.log('                edge = (' + i + ', ' + op + ')');

						graph.addEdge(op, i);
						graph.addEdge(i, op);
						op = stack.pop();

						console.log('        stack = ' + stack.join(', '));
					}
					
					if(tokens[op-1].tag == this.TAGS.optional) {
						console.log('            op = optional');
						console.log('                edge = (' + op + ', ' + i + ')');

						graph.addEdge(op, i);
						op = stack.pop();

						console.log('        stack = ' + stack.join(', '));
					}

					if(tokens[op-1].tag == this.TAGS.end) {
						console.log('            op = end');
						if(or) {console.log('                has or! edge = (' + op + ', ' + (or-1) + ')'); graph.addEdge(op, or-1);}
						rp = op;
					}
				}


				if( tokens[i-1].tag == this.TAGS.end || 
					tokens[i-1].tag == this.TAGS.closure || 
					tokens[i-1].tag == this.TAGS.optional || 
					tokens[i-1].tag == this.TAGS.start ) { graph.addEdge(i, i-1); }
			}

			return graph;
		}
	};
})

.factory('gMatcher', ['gMatcherUtils', function(gMatcherUtils) {
	var gMatcher = function(expression, grammar) {
		var _grammar = grammar;
		var _tokens = gMatcherUtils.tokenize(expression, _grammar);
		var _graph = gMatcherUtils.parse(_tokens);						// non deterministic state machine with epsilon transitions only.

		_graph.print();

		//-----------------------------------------------------
		//	Instance Functions
		//-----------------------------------------------------
		// Match a text against this regex.
		this.match = function(text) {
			//console.log("############################");
			//console.log("# Matching");
			//console.log("############################");
			//console.log("|    text = " + text);

			var i, j, v, w;

			// Get states reachable from start through epsilon transitions.
			var stateIdxs = [];
			var dfs = new DirectedDFS(_graph, 0);

			for(v = 0; v < _graph.numVertices(); v++) {
				if(dfs.marked(v)) {stateIdxs.push(v);}
			}

			//console.log("|    Reachable = " + stateIdxs.join(", "));

			// Iterate over the text characters.
			for(i = 0; i < text.length; i++) {
				var matched = [];

				//console.log("|        text[" + i + "] = " + text[i]);

				// Search for matches within current reachable states.
				for(j = 0; j < stateIdxs.length; j++) {
					v = stateIdxs[j];

					//console.log("|            state = " + v);
					if(v < _tokens.length) {
						//console.log("|                < _tokens.length");

						if(_tokens[v].tag == gMatcherUtils.TAGS.pattern && text[i].match(_tokens[v].value)) {
							//console.log("|                pattern matched: " + _tokens[v].value);
							matched.push(v+1);
						}
						else if(_tokens[v].tag == gMatcherUtils.TAGS.symbol && text[i] == _tokens[v].value) {
							//console.log("|                symbol matched: " + _tokens[v].value);
							matched.push(v+1);
						}
					}
				}

				//console.log("|        Matched Array = " + matched.join(", "));

				// Get states reachable from the matched states through epsilon transitions.
				stateIdxs = [];
				dfs = new DirectedDFS(_graph, matched);

				for(v = 0; v < _graph.numVertices(); v++) {
					if(dfs.marked(v)) {stateIdxs.push(v);}
				}

				//console.log("|    Reachable = " + stateIdxs.join(", "));
			}

			// Check if there is a accepted state within final reachable states.
			for(i = 0; i < stateIdxs.length; i++) {
				if(stateIdxs[i] == _tokens.length) {return true;}
			}

			return false;
		};
	};

	return gMatcher;
}]);
/**
 * Taken from angular ui bootstrap project.
 * angular-ui.github.io/bootstrap/
 */
angular.module('grass.utils.position', [])

/**
* A set of utility methods that can be use to retrieve position of DOM elements.
* It is meant to be used where we need to absolute-position DOM elements in
* relation to other, existing elements (this is the case for tooltips, popovers,
* typeahead suggestions etc.).
*/
.factory('$position', ['$document', '$window', function ($document, $window) {

	function getStyle(el, cssprop) {
		if (el.currentStyle) { //IE
			return el.currentStyle[cssprop];
		} else if ($window.getComputedStyle) {
			return $window.getComputedStyle(el)[cssprop];
		}
		// finally try and get inline style
		return el.style[cssprop];
	}

	/**
	 * Checks if a given element is statically positioned
	 * @param element - raw DOM element
	 */
	function isStaticPositioned(element) {
		return (getStyle(element, "position") || 'static' ) === 'static';
	}

	/**
	 * returns the closest, non-statically positioned parentOffset of a given element
	 * @param element
	 */
	var parentOffsetEl = function (element) {
		var docDomEl = $document[0];
		var offsetParent = element.offsetParent || docDomEl;
		while (offsetParent && offsetParent !== docDomEl && isStaticPositioned(offsetParent) ) {
			offsetParent = offsetParent.offsetParent;
		}
		return offsetParent || docDomEl;
	};

	return {
		/**
		 * Provides read-only equivalent of jQuery's position function:
		 * http://api.jquery.com/position/
		 */
		position: function (element) {
			var elBCR = this.offset(element);
			var offsetParentBCR = { top: 0, left: 0 };
			var offsetParentEl = parentOffsetEl(element[0]);
			if (offsetParentEl != $document[0]) {
				offsetParentBCR = this.offset(angular.element(offsetParentEl));
				offsetParentBCR.top += offsetParentEl.clientTop - offsetParentEl.scrollTop;
				offsetParentBCR.left += offsetParentEl.clientLeft - offsetParentEl.scrollLeft;
			}

			var boundingClientRect = element[0].getBoundingClientRect();
			return {
				width: boundingClientRect.width || element.prop('offsetWidth'),
				height: boundingClientRect.height || element.prop('offsetHeight'),
				top: elBCR.top - offsetParentBCR.top,
				left: elBCR.left - offsetParentBCR.left
			};
		},

		/**
		 * Provides read-only equivalent of jQuery's offset function:
		 * http://api.jquery.com/offset/
		 */
		offset: function (element) {
			var boundingClientRect = element[0].getBoundingClientRect();
			return {
				width: boundingClientRect.width || element.prop('offsetWidth'),
				height: boundingClientRect.height || element.prop('offsetHeight'),
				top: boundingClientRect.top + ($window.pageYOffset || $document[0].body.scrollTop || $document[0].documentElement.scrollTop),
				left: boundingClientRect.left + ($window.pageXOffset || $document[0].body.scrollLeft  || $document[0].documentElement.scrollLeft)
			};
		}
	};
}]);
/**
 * Taken from angular ui bootstrap project.
 * angular-ui.github.io/bootstrap/
 */
angular.module('grass.utils.transition', [])

.factory('$transition', ['$q', '$timeout', '$rootScope', function($q, $timeout, $rootScope) {

	var $transition = function(element, trigger, options) {
		options = options || {};
		var deferred = $q.defer();
		var endEventName = $transition[options.animation ? "animationEndEventName" : "transitionEndEventName"];

		var transitionEndHandler = function(event) {
			$rootScope.$apply(function() {
				element.unbind(endEventName, transitionEndHandler);
				deferred.resolve(element);
			});
		};

		if (endEventName) {
			element.bind(endEventName, transitionEndHandler);
		}

		// Wrap in a timeout to allow the browser time to update the DOM before the transition is to occur
		$timeout(function() {
			if ( angular.isString(trigger) ) {
				element.addClass(trigger);
			} else if ( angular.isFunction(trigger) ) {
				trigger(element);
			} else if ( angular.isObject(trigger) ) {
				element.css(trigger);
			}
			//If browser does not support transitions, instantly resolve
			if ( !endEventName ) {
				deferred.resolve(element);
			}
		});

		// Add our custom cancel function to the promise that is returned
		// We can call this if we are about to run a new transition, which we know will prevent this transition from ending,
		// i.e. it will therefore never raise a transitionEnd event for that transition
		deferred.promise.cancel = function() {
			if ( endEventName ) {
				element.unbind(endEventName, transitionEndHandler);
			}
			deferred.reject('Transition cancelled');
		};

		return deferred.promise;
	};

	// Work out the name of the transitionEnd event
	var transElement = document.createElement('trans');
	var transitionEndEventNames = {
		'WebkitTransition': 'webkitTransitionEnd',
		'MozTransition': 'transitionend',
		'OTransition': 'oTransitionEnd',
		'transition': 'transitionend'
	};
	var animationEndEventNames = {
		'WebkitTransition': 'webkitAnimationEnd',
		'MozTransition': 'animationend',
		'OTransition': 'oAnimationEnd',
		'transition': 'animationend'
	};
	function findEndEventName(endEventNames) {
		for (var name in endEventNames){
			if (transElement.style[name] !== undefined) {
				return endEventNames[name];
			}
		}
	}
	$transition.transitionEndEventName = findEndEventName(transitionEndEventNames);
	$transition.animationEndEventName = findEndEventName(animationEndEventNames);
	return $transition;
}]);