module.exports = {
	production_dir: 'dist',

	app_files: {
		js: 		['src/grass', 'src/**/*.js', '!src/**/*.spec.js'],
		js_unit: 	['src/**/*.spec.js']
	}
};