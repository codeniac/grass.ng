var __ = require('underscore');

module.exports = function(grunt) {

	/**
	 * Load the plugins that provides the tasks.
	 */
	grunt.loadNpmTasks('grunt-contrib-clean');
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-watch');

	/**
	 * Configuration
	 */
	var grunt_config = require( './grunt.config.js' );

	//####################################################################################################
	//										Tasks Configuration
	//####################################################################################################
	var tasks_config = {
		pkg: grunt.file.readJSON('package.json'),

		meta: {
			banner: 
				'/**\n' +
				' * <%= pkg.name %> - v<%= pkg.version %>\n' +
				' * <%= pkg.homepage %>\n' +
				' *\n' +
				' * Created by <%= pkg.author %> on <%= grunt.template.today("yyyy-mm-dd") %>\n' +
				' * Copyright (c) <%= grunt.template.today("yyyy") %> <%= pkg.company %>. All rights reserved.\n' +
				' */\n'
		},

		/**
		 * Clean task configuration.
		 */
		clean: {
			prod: [ '<%= production_dir %>' ],
			options: {
				force: true
			}
		},

		/**
		 * JSHint task configuration.
		 */
		jshint: {
			src: [
				'<%= app_files.js %>'
			],

			test: [
				'<%= app_files.js_unit %>'
			],

			gruntfile: [
				'Gruntfile.js'
			],

			options: {
				curly: true,
				immed: true,
				newcap: false,
				noarg: true, 
				sub: true,
				boss: true,
				eqnull: true
			},

			globals: {}
		},

		/**
		 * Concat task configuration.
		 */
		concat: {
			prod: {
				src: [
					'<%= app_files.js %>'
				],
				dest: '<%= production_dir %>/<%= pkg.name %>.js',

				options: {
					banner: '<%= meta.banner %>'
				}
			}
		},

		/**
		 * Uglify task configuration.
		 */
		uglify: {
			prod: {
				files: {
					'<%= production_dir %>/<%= pkg.name %>.min.js': '<%= concat.prod.dest %>',
				},

				options: {
					banner: '<%= meta.banner %>',
					compress: {
						drop_console: true
					}
				}
			}
		},

		/**
		 * Watch task configuration.
		 */
		delta: {
			options: {
				livereload: 35730
			},

			js: {
				files: ['<%= app_files.js %>'],
				tasks: ['jshint', 'concat:prod', 'uglify:prod']
			}
		}
	};


	//####################################################################################################
	//										Grunt Configuration
	//####################################################################################################
	grunt.initConfig( __.extend( tasks_config, grunt_config ) );


	//####################################################################################################
	//										Tasks Registration
	//####################################################################################################
	/**
	 * Watch Task
	 */
	grunt.renameTask( 'watch', 'delta' );
	grunt.registerTask( 'watch', [ 'default', 'delta' ] );

	/**
	 * Default Task
	 */
	grunt.registerTask( 'default', [
		'clean:prod',
		'jshint',
		'concat:prod',
		'uglify:prod'
	]);
};